# Tools Library

Contains guidelines for installing and configuring all the needed embedded development tools for the Embedded Academy and Embedded Internship.

Mandatory guidelines for the Embedded Internship:
1. Sourcetree
2. Eclipse
3. GNUEclipseMCU
4. Python

Mandatory guidelines for the Embedded Academy:
1. Sourcetree
2. Notepad++
3. BeyondCompare
4. 7-Zip
5. FreeCommanderXE
6. MinGW
7. Eclipse
8. GNUEclipseMCU
9. Python
10. Picoscope

Optional guidelines for the Embedded Academy:
1. KiCad
2. SerialTerminal